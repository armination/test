# we are going to start

from PIL import Image
import matplotlib.pyplot as plt

image_path='C:\\Users\Asus\OneDrive\Desktop'
image_name='\eyes.jpg'
image = Image.open(image_path+image_name)

resized=(400,400)
image_resized = image.resize(resized)

print(f"Original size : {image.size}")
print(f"Resized size : {image_resized.size}")

fig = plt.figure(figsize=(8, 8))
fig.add_subplot(1, 2, 1)
plt.imshow(image)
fig.add_subplot(1, 2, 2)
plt.imshow(image_resized)
plt.show()

resized_path="C:\\Users\Asus\OneDrive\Desktop"
resized_name="/resied_image.jpg"
image_resized.save(resized_path+resized_name)